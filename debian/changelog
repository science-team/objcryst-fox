objcryst-fox (2022.1-1) unstable; urgency=medium

  * Team upload.
  * Update homepage and download locations (Closes: #999576)
  * Update to new upstream release 2022.1 (Closes: #781717)
  * Build against unbundled cctbx
  * Transition to wxWidgets 3.2 (Closes: #1019779)
  * Move to Science Team and use VCS
  * Update debhelper-compat level to 13
  * Rules-Requires-Root: no
  * Remove trailing whitespace in d/changelog
  * Update Standards-Version to 4.6.1 (no changes needed)
  * Use dh sequencer (Closes: #960094)

 -- Scott Talbert <swt@techie.net>  Thu, 03 Nov 2022 11:03:36 -0400

objcryst-fox (1.9.6.0-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with GCC 6 (Closes: #811919)
  * Rebuild with wxWidgets GTK3 variant (Closes: #933479)

 -- Scott Talbert <swt@techie.net>  Wed, 06 May 2020 19:33:38 -0400

objcryst-fox (1.9.6.0-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update to use wxwidgets3.0 (new patch wx3.0-compat.patch) (Closes: #749870)
  * debian/control: 'polyhedras' -> 'polyhedra' in the description ('polyhedra'
    is the plural form of 'polyhedron').

 -- Olly Betts <olly@survex.com>  Sun, 24 Aug 2014 15:56:27 +1200

objcryst-fox (1.9.6.0-2) unstable; urgency=low

  * Remove -march=native flag from Objcryst/rules-gnu.mak (Closes: #631653

 -- Carlo Segre <segre@debian.org>  Sat, 25 Jun 2011 20:08:34 -0500

objcryst-fox (1.9.6.0-1) unstable; urgency=low

  * New upstream release
  * Upgrade to source format 3.0 (quilt)
  * Upgrade to Standards-Version 3.9.2 (no changes)

 -- Carlo Segre <segre@debian.org>  Thu, 23 Jun 2011 14:55:51 -0500

objcryst-fox (1.9.0.2-1) unstable; urgency=low

  * New upstream release
  * Upgrade to Standards-Version 3.8.4 (no changes)

 -- Carlo Segre <segre@debian.org>  Sat, 13 Feb 2010 16:48:57 -0600

objcryst-fox (1.8.2~r1158-1) unstable; urgency=low

  * New upstream release
  * Now compiles with gcc-4.4/g++-4.4 (Closes: #505711)
  * Update to Standards-Version 3.8.2 (no changes)
  * Add desktop file with top level Education category.
    NOTE: This is not the right solution but conforms to the freedesktop.org
          standard. KDE and Gnome provide a "Science" top level menu through
          their own configuration files.  XFCE4 currently does not but can be
          configured to do so.  See README.Debian for this information.

 -- Carlo Segre <segre@debian.org>  Sun, 05 Jul 2009 01:23:28 -0500

objcryst-fox (1.8.1.2-2) unstable; urgency=low

  * Correct debhelper version to (>= 7)
  * Correct reference to GPL-2 license.

 -- Carlo Segre <segre@debian.org>  Tue, 26 May 2009 22:01:15 -0500

objcryst-fox (1.8.1.2-1) unstable; urgency=low

  * New upstream release
  * Fix debian/watch file
  * Add Homepage field
  * Include boost libraries distributed with source and compile with
    shared-boost=0 to avoid errors. Remove Build-Dep on libboost-dev,
    libboost-date-time-dev.
  * No need for Build-Dep on libboost-math-dev with shared-boost=0
    (Closes: #530472).
  * Update to Standards-Version 3.8.1 (no changes).

 -- Carlo Segre <segre@debian.org>  Mon, 25 May 2009 17:38:38 -0500

objcryst-fox (1.8.0.R1087-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Thu, 05 Feb 2009 21:10:38 -0600

objcryst-fox (1.8.0.R1085-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Wed, 04 Feb 2009 22:29:04 -0600

objcryst-fox (1.8.0.R1080-1) unstable; urgency=low

  * New upstream release
  * Correct debian/watch file to properly query SourceForge

 -- Carlo Segre <segre@debian.org>  Sun, 25 Jan 2009 15:20:50 -0600

objcryst-fox (1.7.99.R1047-1) unstable; urgency=low

  * First Debian release (Closes: #492388)
  * Changed name to avoid collision with other packages already in Debian.
  * Build with all shared libraries except for cctbx and remove extra
    source tarballs.

 -- Carlo Segre <segre@debian.org>  Mon, 01 Sep 2008 15:25:37 -0500

fox (1.7.7.0-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Mon, 21 Jul 2008 17:11:55 -0500

fox (1.7.5.3-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Tue, 01 Apr 2008 02:10:36 -0500

fox (1.7.0-1) unstable; urgency=low

  * New upstream release, must build statically with included
    tarballs because of dependence on libwxgtk2.8 which is not in
    Debian yet.

 -- Carlo Segre <segre@debian.org>  Wed, 14 Mar 2007 15:08:35 -0500

fox (1.6.0.2-2) unstable; urgency=low

  * Change dependency to libwxgtk2.4-1.
  * Patches to mv VFNStreamFormat.h to be compatible with g++ 4.0

 -- Carlo Segre <segre@iit.edu>  Tue,  2 Aug 2005 00:12:26 -0500

fox (1.6.0.2-1) unstable; urgency=low

  * Initial Release.

 -- Carlo Segre <segre@iit.edu>  Sat, 19 Mar 2005 01:27:55 -0600
